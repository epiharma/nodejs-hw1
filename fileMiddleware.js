const {isLockedFile, compareFilePassword, message} = require('./helpers.js');

const isLocked = (req, res, next) => {
    const {filename} = req.params;
    const {password} = req.query;

    try {
        if (isLockedFile(filename)) {
            if (compareFilePassword(filename, password)) {
                next();
            }

            return res.status(403).json(message('Access denied'));
        } else {
            next();
        }
    } catch (e) {
        return res.status(500).json(message('Server error'));
    }
}

module.exports = {
    isLocked
}