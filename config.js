module.exports = {
    PORT: 8080,
    HOST: 'localhost',
    STORAGE_DIR:  './files/',
    LOCKED_FILES_FILE: 'lockedFiles.json',
    SUPPORTED_FILE_EXTENSIONS: ['log','txt','json','yaml','xml','js'],
    DEFAULT_FILE_ENCODING: 'utf8'
}