const express = require('express');
const router = express.Router();

const {
    isLocked
} = require('./fileMiddleware.js');

const {
    createFile,
    getFiles,
    getFile,
    updateFile,
    deleteFile
} = require('./fileController.js');

router.post('/', createFile);
router.get('/', getFiles);
router.get('/:filename', isLocked, getFile);
router.put('/:filename', isLocked, updateFile);
router.delete('/:filename', isLocked, deleteFile);

module.exports = router;