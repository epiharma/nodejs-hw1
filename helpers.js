const fs = require('fs');
const path = require('path');

const {
    LOCKED_FILES_FILE,
    SUPPORTED_FILE_EXTENSIONS,
    DEFAULT_FILE_ENCODING,
    STORAGE_DIR
} = require('./config.js');

const exists = (path) => {
    return fs.existsSync(path);
}

const createDirIfNotExists = () => {
    if (!exists(STORAGE_DIR)){
        fs.mkdirSync(STORAGE_DIR);
    }
}

const getFileExtension = (filename) => {
    return path.extname(filename).replace('.', '');
}

const isValidFileName = (filename) => {
    let regEx1 = /^[^\\/:\*\?"<>\|]+$/;
    let regEx2 = /^\./;

    return regEx1.test(filename) && !regEx2.test(filename);
}

const isValidFileExtension = (filename) => {
    return SUPPORTED_FILE_EXTENSIONS.includes(getFileExtension(filename));
}

const registerNewProtectedFile = (filename, password) => {
    let files = fs.readFileSync(LOCKED_FILES_FILE, DEFAULT_FILE_ENCODING);
    files = JSON.parse(files);
    files[filename] = {name: filename, password: password};

    files = JSON.stringify(files);
    fs.writeFileSync(LOCKED_FILES_FILE, files);
}

const isLockedFile = (filename) => {
    let files = fs.readFileSync(LOCKED_FILES_FILE, DEFAULT_FILE_ENCODING);
    files = JSON.parse(files);

    return files.hasOwnProperty(filename);
}

const compareFilePassword = (filename, receivedPassword) => {
    let files = fs.readFileSync(LOCKED_FILES_FILE, DEFAULT_FILE_ENCODING);
    files = JSON.parse(files);

    return files[filename].password === receivedPassword;
}

const message = (messageText, additionalData) => {
    return {message: messageText, ...additionalData};
}

module.exports = {
    exists,
    createDirIfNotExists,
    getFileExtension,
    isValidFileName,
    isValidFileExtension,
    registerNewProtectedFile,
    isLockedFile,
    compareFilePassword,
    message
}