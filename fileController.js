const fs = require('fs');
const path = require('path');

const {
    STORAGE_DIR,
    DEFAULT_FILE_ENCODING
} = require('./config.js');

const {
    createDirIfNotExists,
    isValidFileExtension,
    isValidFileName,
    exists,
    registerNewProtectedFile,
    message
} = require('./helpers.js');


const createFile = (req, res) => {
    const {filename, content, password} = req.body;

    if (!filename) {
        return res.status(400).json(message("Please specify 'filename' parameter"));
    }

    if (!content) {
        return res.status(400).json(message("Please specify 'content' parameter"));
    }

    if(!isValidFileName(filename)){
        return res.status(400).json(message('Inappropriate file name'));
    }

    if (!isValidFileExtension(filename)) {
        return res.status(400).json(message('Unsupported file extension'));
    }

    try {
        createDirIfNotExists();

        if (exists(path.join(STORAGE_DIR, filename))) {
            return res.status(400).json(message('File already exists'));
        }

        fs.writeFileSync(path.join(STORAGE_DIR, filename), content, DEFAULT_FILE_ENCODING);

        if (password) {
            registerNewProtectedFile(filename, password);
        }

        return res.status(200).json(message('File created successfully'));
    } catch (e) {
        return res.status(500).json(message('Server error'));
    }
}

const getFile = (req, res) => {
    const {filename} = req.params;
    let fileInfo = {filename};

    fileInfo.extension = path.extname(filename).replace('.', '');

    try {
        if (!exists(path.join(STORAGE_DIR, filename))) {
            return res.status(400).json(message(`No file with ${filename} filename found`));
        }

        fileInfo.content = fs.readFileSync(path.join(STORAGE_DIR, filename), DEFAULT_FILE_ENCODING);
        fileInfo.uploadedDate = fs.statSync(path.join(STORAGE_DIR, filename)).birthtime;
    } catch (e) {
        return res.status(500).json(message('Server error'));
    }

    return res.status(200).json(message('Success', fileInfo));
}

const getFiles = (req, res) => {
    try {
        createDirIfNotExists();

        const files = fs.readdirSync(STORAGE_DIR, DEFAULT_FILE_ENCODING);

        return res.status(200).json(message('Success', {'files': files}));
    } catch (e) {
        return res.status(500).json(message('Server error'));
    }
}

const updateFile = (req, res) => {
    const {filename} = req.params;
    const {content} = req.body;

    if (!filename) {
        return res.status(400).json(message("Please specify 'filename' parameter"));
    }

    if (!content) {
        return res.status(400).json(message("Please specify 'content' parameter"));
    }

    try {
        if (!exists(path.join(STORAGE_DIR, filename))) {
            return res.status(400).json(message(`No file with ${filename} filename found`));
        }

        fs.writeFileSync(path.join(STORAGE_DIR, filename), content, DEFAULT_FILE_ENCODING);

        return res.status(200).json(message('File successfully updated'));
    } catch (e) {
        return res.status(500).json(message('Server error'));
    }
}

const deleteFile = (req, res) => {
    const {filename} = req.params;

    try {
        if (!exists(path.join(STORAGE_DIR, filename))) {
            return res.status(400).json(message(`No file with ${filename} filename found`));
        }

        fs.unlinkSync(path.join(STORAGE_DIR, filename));

        return res.status(200).json(message('File successfully removed'));
    } catch (e) {
        return res.status(500).json(message('Server error'));
    }
}

module.exports = {
    createFile,
    getFile,
    getFiles,
    updateFile,
    deleteFile
}