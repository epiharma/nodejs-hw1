const express = require('express');
const morgan = require('morgan');

const app = express();

const {PORT} = require('./config.js');

const fileRouter = require('./fileRouter.js');

app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/files', fileRouter);

app.listen(PORT, () => {
    console.log(`Server works at port ${PORT}!`);
});